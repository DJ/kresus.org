Title: Contributing
Date: 2017-03-11 10:02
Slug: contribute
Summary: How to contribute to Kresus?
lang: en

# How to contribute to Kresus

Kresus is a <a
href="https://framagit.org/kresusapp/kresus/blob/master/LICENSE"><strong><em>libre</em></strong></a>
and <a
href="https://framagit.org/kresusapp/kresus"><strong>open-source</strong></a>
software.
You can then contribute easily, whatever your skills!

Here are some ideas to get you started:

* Report any issue you might find in our [bugs
  tracker](https://framagit.org/kresusapp/kresus/issues).
* Choose a ["good first
  bug"](https://framagit.org/kresusapp/kresus/issues?label_name%5B%5D=good+first+bug)
  and fix it. These
  [guidelines](https://framagit.org/kresusapp/kresus/blob/master/CONTRIBUTING.md)
  to contribute might be useful.
* Enhance design or user interface of Kresus.
* [Translate Kresus](https://framagit.org/kresusapp/kresus/tree/master/shared/locales) in other languages.
* Write some documentation (this website is <em>open-source</em> too: [https://framagit.org/kresusapp/kresus.org](https://framagit.org/kresusapp/kresus.org) !)
* And simply talk about Kresus around you!

<em>Note:</em> Kresus contributors have set up a code of conduct so that
everyone is welcome to contribute. You should [read
it](https://framagit.org/kresusapp/kresus/blob/master/CodeOfConduct.md) first.
